import yaml

REQUIRED = ['s3', 'sentry_dsn', 'users']
def parse():
    with open('/etc/rugosa.conf', 'r') as f:
        config = yaml.safe_load(f)
    for prop in REQUIRED:
        if prop not in config:
            raise Exception("You must supply {} in your configuration file".format(prop))
    return config
