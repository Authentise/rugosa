import asyncio
import aiohttp.web
import base64
import datetime
import functools
import json
import logging
import rugosa.version
import yieldfrom.botocore.exceptions
import yieldfrom.botocore.session

LOGGER = logging.getLogger('server')

class Server(object):
    def __init__(self, app, config):
        self.app         = app
        self.botosession = yieldfrom.botocore.session.get_session()
        self.cache       = {}
        self.config      = config
        self.s3client    = None

    @asyncio.coroutine
    def connect(self):
        self.s3client    = yield from self.botosession.create_client('s3', region_name=self.config['s3']['region'])

    @asyncio.coroutine
    def get_file_content(self, path):
        now = datetime.datetime.utcnow()
        try:
            value, time = self.cache[path]
            if (now - time).total_seconds() < self.config['s3']['cache']:
                return value
        except KeyError:
            pass

        LOGGER.debug("Fetching %s", path)
        response = yield from self.s3client.get_object(
            Bucket=self.config['s3']['bucket'],
            Key=path)
        body = yield from response['Body'].read()
        result = body, response['ContentType']

        self.cache[path] = result, now
        return result

@asyncio.coroutine
def init(config, loop):
    app = aiohttp.web.Application(loop=loop)
    server = Server(app, config)
    yield from server.connect()

    app.router.add_route('GET', '/{path:.*}', functools.partial(root, server))

    host = '127.0.0.1'
    port = 5000
    srv = yield from loop.create_server(app.make_handler(), host, port)
    LOGGER.info("Server started at %s:%s", host, port)
    return srv

def start(config):
    LOGGER.info("Starting rugosa %s", rugosa.version.VERSION)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(init(config, loop))
    loop.run_forever()

def _bad_auth_response(config):
    realm = 'Basic realm="{}"'.format(config.get('realm', 'rugosa'))
    return aiohttp.web.Response(
        body='Authentication Required'.encode('utf-8'),
        status=401,
        headers={'WWW-Authenticate': realm})

def _matches_user(config, username, password):
    for _u, _p in config['users'].items():
        if username == _u and password == _p:
            return True

def auth_required(func):
    @functools.wraps(func)
    def _inner(server, request, **kwargs):
        auth = request.headers.get('Authorization', None)
        if auth is None:
            return _bad_auth_response(server.config)
        auth_type, _, content = auth.partition(' ')
        if auth_type.lower() != 'basic':
            return aiohttp.web.Response(
                body="Bad authorization type. I do not recognize '{}' as an authorization".format(auth_type).encode('utf-8'),
                status=400)
        decoded = base64.b64decode(content).decode('utf-8')
        username, _, password = decoded.partition(':')
        if _matches_user(server.config, username, password):
            return func(server, request, **kwargs)
        else:
            return _bad_auth_response(server.config)
    return _inner

def error_response(status_code, error_code, description):
    return aiohttp.web.Response(body=description.encode('utf-8'), status=status_code, headers={'X-Api-Error': error_code})

def json_response(payload, status_code=200):
    body = json.dumps(payload).encode('utf-8')
    return aiohttp.web.Response(body=body, status=status_code, headers={'Content-Type': 'application/json'})

@auth_required
def root(server, request):
    path = request.match_info.get('path', None)
    path = 'index.html' if not path else path
    try:
        content, _type = yield from server.get_file_content(path)
    except yieldfrom.botocore.exceptions.ClientError:
        return aiohttp.web.Response(body='Not found'.encode('utf-8'), status=404)
    return aiohttp.web.Response(
        body=content,
        status=200,
        headers={
            'Cache-Control' : 'max-age={}'.format(server.config['s3']['cache']),
            'Content-Type'  : _type,
        })

