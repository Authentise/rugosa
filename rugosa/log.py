import logging
import logging.config
import raven
import raven.handlers.logging
import raven.conf

def setup_logging():
    logging.config.dictConfig({
        'version'   : 1,
        'disable_existing_loggers'  : False,
        'formatters'                : {
            'standard'              : {
                'format'            : '[%(asctime)s] %(levelname)s pid:%(process)d %(name)s:%(lineno)d %(message)s',
                'dateformat'        : '%d/%b/%Y:%H:%M:%S %z',
            },
        },
        'handlers'                  : {
            'default'               : {
                'level'             : 'DEBUG',
                'class'             : 'logging.StreamHandler',
                'formatter'         : 'standard',
            },
        },
        'loggers'                   : {
            ''                      : {
                'handlers'          : ['default'],
                'level'             : 'DEBUG',
                'propogate'         : True,
            },
        },
    })

def setup_raven(dsn):
    if not dsn:
        return

    sentry_client = raven.Client(dsn)
    sentry_handler = raven.handlers.logging.SentryHandler(sentry_client)
    sentry_handler.setLevel(logging.ERROR)
    raven.conf.setup_logging(sentry_handler)
