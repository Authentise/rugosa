set -x
PROJECT=rugosa
SCRIPTS="bin/rugosa"
mkdir -p dist
if [ -z "$1" ]; then
    FILES="$(find $PROJECT -maxdepth 4 -name "*.py" -not -path "*alembic/*" -print) $SCRIPTS $(find tests -maxdepth 4 -name "*.py" -print)"
else
    FILES="$1"
    echo "linting $FILES"
fi
pylint --rcfile=tests/pylint.cfg $FILES --reports=no | tee dist/pylint.log
