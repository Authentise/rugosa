#!/bin/bash -vx
set -e
rm -f dist/*

echo "Building the python package"
rm -Rf ./dist
mkdir -p dist
./pylint.sh
py.test --cov-report xml --cov rugosa --junitxml dist/results.xml tests
VERSION=$VERSION python setup.py sdist
